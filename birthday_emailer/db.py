import sqlite3


class DB:
    def __init__(self, birthdays_data_path, debug=False):
        self.connection = sqlite3.connect(":memory:")
        self.cursor = self.connection.cursor()
        self.cursor.execute(
            """CREATE TABLE birthdays
               (last_name text, first_name text, email text, day text)"""
        )
        with open(birthdays_data_path, "r") as fh:
            for l in fh:
                if l.strip():
                    vals = ", ".join([f"'{v}'" for v in l.strip().split(";")])
                    self.cursor.execute(f"INSERT INTO birthdays VALUES ({vals})")
        self.connection.commit()

    def get_days_entries(self, day):
        return list(
            self.cursor.execute("SELECT * FROM birthdays WHERE day=:day", {"day": day})
        )


if __name__ == "__main__":
    db = DB("../birthdays.csv")
    print(db.get_days_entries("0721"))
