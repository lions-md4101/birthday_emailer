import emailer

with open(
    f"../template.txt",
    "r",
) as fh:
    template = fh.read()
    message = template.format(first_name="Kim")

emailer.send_mail(
    "vanwykk@gmail.com",
    message,
    bccs=["vanwykk+birthdays@gmail.com"],
)
