from os import getenv

from dotenv import load_dotenv
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import (
    Mail,
    MimeType,
    Content,
    Bcc,
)

load_dotenv()

MAILER = SendGridAPIClient(getenv("SENDGRID_API_KEY"))
BCCS = [a for a in getenv("EMAIL_BCCS").split(";") if a]

FROM_MAIL = ("birthdays@lions410e.org.za", "Lions 410E Birthday Messages")


def send_mail(
    recipients,
    body,
    bccs=BCCS,
):
    if not hasattr(recipients, "extend"):
        recipients = [recipients]
    recipients = [(r, r) for r in recipients]

    message = Mail(
        from_email=FROM_MAIL,
        to_emails=recipients,
        subject="Happy Birthday!",
    )
    if bccs:
        message.bcc = [Bcc(bcc, bcc, p=0) for bcc in bccs]
    message.content = Content(MimeType.html, body)
    response = MAILER.send(message)


if __name__ == "__main__":
    send_mail(
        "vanwykk@gmail.com",
        "This is the <b>body</b>",
        bccs=["vanwykk+birthdays@gmail.com"],
    )
