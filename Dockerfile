FROM registry.gitlab.com/kimvanwyk/python3-poetry

# /app is the workdir. Copy files to execute to /app/
COPY ./birthday_emailer /app/

ENTRYPOINT ["python", "-u", "scheduler.py"]
