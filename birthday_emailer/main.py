from datetime import date
from os import getenv

import db
import emailer

CONFIG_DIR = getenv("CONFIG_DIR")

DBH = db.DB(f"{CONFIG_DIR}/birthdays.csv")
with open(f"{CONFIG_DIR}/template.txt") as fh:
    TEMPLATE = fh.read()


def get_day():
    return date.today().strftime("%m%d")


def get_recipients():
    day = get_day()
    return DBH.get_days_entries(day)


def email_recipients(recipients):
    for (_, first_name, email, _) in recipients:
        message = TEMPLATE.format(first_name=first_name)
        emailer.send_mail(
            email,
            message,
        )


def main():
    email_recipients(get_recipients())
