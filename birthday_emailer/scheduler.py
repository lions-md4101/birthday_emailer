from os import getenv
import time

import main

import schedule

schedule.every().day.at(getenv("TIME")).do(main.main)
while True:
    schedule.run_pending()
    time.sleep(1)
